#!/usr/bin/env bash

set -e

find ./crawl -type f -print0 |
    while IFS= read -d '' file_name; do
        mv "$file_name" "${file_name/%\?*/}"
    done

