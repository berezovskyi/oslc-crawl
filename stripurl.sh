#!/usr/bin/env bash

set -e

find ./crawl -type f -print0 | xargs -0 sed -i 's#http://open-services.net/#/#g'
find ./crawl -type f -print0 | xargs -0 sed -i 's#https://open-services.net/#/#g'
