#!/usr/bin/env ruby

# See https://stackoverflow.com/questions/166347/how-do-i-use-ruby-for-shell-scripting?rq=1
# and https://scotch.io/tutorials/a-crash-course-in-ruby

puts "Starting to post-process open-services.net crawl"

# http://open-services.net/minimee_cache/ had dirlisting turned on so I went
# for the bruteforce solution
puts "Step 1: copy the missed minimee resources"

%x( cp -R minimee_cache crawl/open-services.net )

puts "Step 2: rename all files with .css?#### or .js?#### to a canonical form"

#%x(bash rmquery.sh)
puts "SKIPPING"

puts "Step 3: convert all remaining open-services.net URIs to absolute URIs w/o host"

%x(bash stripurl.sh)

puts "\nSUCCESS! To test locally, run\n\n(cd crawl/open-services.net/ && python3 -m http.server)\n"
