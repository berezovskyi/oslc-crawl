# open-services.net archival crawl

## Crawl

Requires Docker (used v17.09.0-ce):

    # 1 level
    ./crawl.sh 1
    # get'em all
    ./crawl.sh inf

## Post-process

Requires Ruby (used v2.3.0p0):

    ./postprocess.rb

## Serve

    (cd crawl/open-services.net/ && python3 -m http.server)