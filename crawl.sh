#!/usr/bin/env bash

set -e

[ "$#" -eq 1 ] || { echo "Usage: ./convert.sh DEPTH"; exit 1; }

mkdir -p crawl
#docker run -t -i -v "$(pwd)/crawl":"/var/crawl" berezovskyi/wget1.12 \
wget --directory-prefix=crawl \
	   --no-clobber \
		 --level=$1 \
     --recursive \
     --page-requisites \
     --adjust-extension \
     --wait=0 \
     --quota=inf \
     --domains=open-services.net \
		 --reject '*Special:*,*/revision/*' \
		 ‐‐user-agent="Mozilla/5.0 Firefox/4.0.1" \
		 --no-parent \
http://open-services.net
#		 ‐‐keep-session-cookies \
#     --convert-links \
#		 ‐‐execute robots=off \
#		 ‐‐referer=http://google.com \
