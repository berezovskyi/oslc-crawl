# wget 1.12 image

## Why?

In 1.12.1 wget devs ["fixed" the problem](http://savannah.gnu.org/bugs/?31781) that [broke some uses of wget](https://superuser.com/questions/80060/make-wget-convert-html-links-to-relative-after-download-if-k-wasnt-specified). I just needed to run an old version before this bugfix. I did not want to install openssl 0.9.8 into my current system and wget 1.12 depends on it, hence a Docker image off an old Debian Squeeze.

## How to build & push to Docker hub

Note to future self. In order to push to Docker store:

    sudo docker build -t berezovskyi/wget1.12 .
    # sudo docker login
    sudo docker push berezovskyi/wget1.12

